import React, {useState, useEffect} from 'react';
import AdminInterface from '../Calendar/admin_interface/admin_interface_app';

//Get the admin calendar infromation from server. {match} is the table name of admin's information in database.

export default function Display({match}){
  useEffect(() => {
    fetchToken();
  }, []);
  const name = match.params.name;
  const [calendar, setCalendar] = useState([]);
  //Fetch the calendar information from server
  const fetchToken = async () => {
    await fetch("http://localhost:5000/calendarinfo", {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({name})
      })
      .then(res => res.json())
      .then(data => {
        setCalendar(data.value);
      });
  }
  return(
    <div>
      <AdminInterface calendar={calendar} docID={name}/>
    </div>
  )
}
