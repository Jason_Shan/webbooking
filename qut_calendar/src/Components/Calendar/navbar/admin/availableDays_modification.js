import React, {useState, useEffect} from 'react';
import {Button, Modal, ModalHeader, ModalBody, ModalFooter,
        Form, FormGroup, Label, CustomInput} from 'reactstrap';

//This function let the admin to modify the avaliable days which could let client to make a booking.
//In the modal, the days with booking will be displayed as "checked".
//Admin could modify the avaliable status of the check boxes.
const DaysModification = props => {
  //Current available days and time for each type of meetings. Information from data base.
  const meetingCategories = props.availableInfo;
  const meetingGroups = [meetingCategories.ProjectMeeting.split(","), 
                        meetingCategories.StaffMeeting.split(","),
                        meetingCategories.GeneralCourseAdvice.split(","),
                        meetingCategories.StudentCounsultation.split(","),
                        meetingCategories.MiscellaneousMeeting.split(",")];
  const meetingNames = ["ProjectMeeting", 
                        "StaffMeeting", 
                        "GeneralCourseAdvice", 
                        "StudentCounsultation", 
                        "MiscellaneousMeeting"];
  //different types of Meeting avaliable days.
  const [monAvailable, setMonAvailable] = useState(false);
  const [tueAvailable, setTueAvailable] = useState(false);
  const [wedAvailable, setWedAvailable] = useState(false);
  const [thuAvailable, setThuAvailable] = useState(false);
  const [friAvailable, setFriAvailable] = useState(false);
  useEffect(() => {
    checkAvaliable();
  }, []);
  //Check if weekdays have meetings.
  const checkAvaliable = () => {
    meetingGroups.forEach(meeting => {meeting.forEach((day, index) => {
      if(day.length > 7){
        switch(index) {
          case 0:
            setMonAvailable(true);
            break;
          case 1:
            setTueAvailable(true);
            break;
          case 2:
            setWedAvailable(true);
            break;
          case 3:
            setThuAvailable(true);
            break;
          case 4:
            setFriAvailable(true);
            break;
          default:
            break;
        }
      }
    })})
  }
  //Submit the midification to database.
  const handleSubmit = e => {
    e.preventDefault();
    const modification = `Mon${monAvailable? "09:00-17:00": "null"},`+
                         `Tue${tueAvailable? "09:00-17:00": "null"},`+
                         `Wed${wedAvailable? "09:00-17:00": "null"},`+
                         `Thu${thuAvailable? "09:00-17:00": "null"},`+
                         `Fri${friAvailable? "09:00-17:00": "null"}`;
    
    meetingNames.forEach(meeting => {
      const submitInfo = props.docID + "%" + meeting + "%" + modification;
      fetch("http://localhost:5000/availabletimemodify", {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({submitInfo})
        })
      })
  }
  return(
    <Form id='availableDayChange' onSubmit={handleSubmit}>
      <FormGroup>
        <Label for="availableDaysCheckBox">
        <div>
          <CustomInput 
            type="checkbox" 
            id="mon" 
            label="Monday" 
            checked={monAvailable} 
            onChange={() => setMonAvailable(!monAvailable)}/>
          <CustomInput 
            type="checkbox" 
            id="tue" 
            label="Tuesday" 
            checked={tueAvailable}
            onChange={() => setTueAvailable(!tueAvailable)}/>
          <CustomInput 
            type="checkbox" 
            id="wed" 
            label="Wednesday" 
            checked={wedAvailable}
            onChange={() => setWedAvailable(!wedAvailable)}/>
          <CustomInput 
            type="checkbox" 
            id="thu" 
            label="Thursday" 
            checked={thuAvailable}
            onChange={() => setThuAvailable(!thuAvailable)}/>
          <CustomInput 
            type="checkbox" 
            id="fri" 
            label="Friday" 
            checked={friAvailable}
            onChange={() => setFriAvailable(!friAvailable)}/>
        </div>
        </Label>
      </FormGroup>  
    </Form>
  )
}

//
export default function AvailableDaysModification(props){
  const id = props.docID;
  const [availableInfo, setAvailableInfo] = useState([]);
  useEffect(() => {
    fetchAvailableTime();
  }, []);
  //Get the calendar information from server
  const fetchAvailableTime = async () => {
    await fetch("http://localhost:5000/availabletimeinfo", {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({id})
      })
      .then(res => res.json())
      .then(data => {
        setAvailableInfo(data[0]);
      });
  }
  const [modal, setModal] = useState(false);
  const modalToggle = () => {
    setModal(!modal);
  }
  return(
    <div>
      <Button size="sm" color="warning" onClick={() => modalToggle()}>Available Days Modification</Button>
      <Modal isOpen={modal} toggle={modalToggle} size="sm">
        <ModalHeader toggle={() => modalToggle()}>Available Days Modification</ModalHeader>
        <ModalBody><DaysModification availableInfo={availableInfo} docID={id}/></ModalBody>
        <ModalFooter>
        <Button color="danger" onClick={() => modalToggle()}>Close</Button>
        <Button form='availableDayChange' type="submit" color="success" onClick={() => {window.location.href=`http://localhost:3000/mycalendar/${id}`}}>Submit</Button>
        </ModalFooter>
      </Modal>
    </div>
  )
}