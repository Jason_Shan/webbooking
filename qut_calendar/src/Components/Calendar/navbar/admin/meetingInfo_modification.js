import React, {useState, useEffect} from 'react';
import {Button, Modal, ModalHeader, ModalBody, ModalFooter,
        Col, Row, Form, FormGroup, Label, Input, CustomInput} from 'reactstrap';

// Modifying the every type of meeting information.
//Admin could change the start and end time for each type of meeting.
const MeetingModification = props => {

  //Current available days and time for each type of meetings. Information from data base.
  const meetingCategories = props.availableInfo;
  const projectMeeting = meetingCategories.ProjectMeeting.split(",");
  const staffMeeting = meetingCategories.StaffMeeting.split(",");
  const generalCourseAdvice = meetingCategories.GeneralCourseAdvice.split(",");
  const studentCounsultation = meetingCategories.StudentCounsultation.split(",");
  const miscellaneousMeeting = meetingCategories.MiscellaneousMeeting.split(",");
  //different types of Meeting avaliable days.
  const [monAvailable, setMonAvailable] = useState(false);
  const [tueAvailable, setTueAvailable] = useState(false);
  const [wedAvailable, setWedAvailable] = useState(false);
  const [thuAvailable, setThuAvailable] = useState(false);
  const [friAvailable, setFriAvailable] = useState(false);
  //Meeting start and end time for saving.
  const [monStart, setMonStart] = useState("");
  const [tueStart, setTueStart] = useState("");
  const [wedStart, setWedStart] = useState("");
  const [thuStart, setThuStart] = useState("");
  const [friStart, setFriStart] = useState("");
  const [monEnd, setMonEnd] = useState("");
  const [tueEnd, setTueEnd] = useState("");
  const [wedEnd, setWedEnd] = useState("");
  const [thuEnd, setThuEnd] = useState("");
  const [friEnd, setFriEnd] = useState("");

  //Handel change functions, get all user informations.
  const handleMonStartTime = e => {
    setMonStart(e.target.value);
  }
  const handleTueStartTime = e => {
    setTueStart(e.target.value);
  }
  const handleWedStartTime = e => {
    setWedStart(e.target.value);
  }
  const handleThuStartTime = e => {
    setThuStart(e.target.value);
  }
  const handleFriStartTime = e => {
    setFriStart(e.target.value);
  }
  const handleMonEndTime = e => {
    setMonEnd(e.target.value);
  }
  const handleTueEndTime = e => {
    setTueEnd(e.target.value);
  }
  const handleWedEndTime = e => {
    setWedEnd(e.target.value);
  }
  const handleThuEndTime = e => {
    setThuEnd(e.target.value);
  }
  const handleFriEndTime = e => {
    setFriEnd(e.target.value);
  }

  //When the subject change, check the available information, re-render the modifying meeting information page.
  const handleMeetingSubjectChange = e => {
    setMonAvailable(false);
    setTueAvailable(false);
    setWedAvailable(false);
    setThuAvailable(false);
    setFriAvailable(false);
    setMonStart("09:00");
    setTueStart("09:00");
    setWedStart("09:00");
    setThuStart("09:00");
    setFriStart("09:00");
    setMonEnd("17:00");
    setTueEnd("17:00");
    setWedEnd("17:00")
    setThuEnd("17:00");
    setFriEnd("17:00");
    availableDayCheck(e.target.value);
  }

  //The information need to be submitted to the database.
  const handleSubmit = e => {
    e.preventDefault();
    const modification = `Mon${monAvailable? monStart+"-"+monEnd: null},`+
                         `Tue${tueAvailable? tueStart+"-"+tueEnd: null},`+
                         `Wed${wedAvailable? wedStart+"-"+wedEnd: null},`+
                         `Thu${thuAvailable? thuStart+"-"+thuEnd: null},`+
                         `Fri${friAvailable? friStart+"-"+friEnd: null}`;

    const submitInfo = props.docID + "%" + document.getElementById("MeetingSubject").value + "%" + modification;
    fetch("http://localhost:5000/availabletimemodify", {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({submitInfo})
      })
  }

  //Check the subject information.
  const checkSubject = meeting => {
    meeting.forEach((item, index) => {
      if(item.length > 7){
        switch(index) {
          case 0:
            setMonAvailable(true);
            setMonStart(("0" + item.split("-")[0].substr(3)).slice(-5));
            setMonEnd(item.split("-")[1]);
            break;
          case 1:
            setTueAvailable(true);
            setTueStart(("0" + item.split("-")[0].substr(3)).slice(-5));
            setTueEnd(item.split("-")[1]);
            break;
          case 2:
            setWedAvailable(true);
            setWedStart(("0" + item.split("-")[0].substr(3)).slice(-5));
            setWedEnd(item.split("-")[1]);
            break;
          case 3:
            setThuAvailable(true);
            setThuStart(("0" + item.split("-")[0].substr(3)).slice(-5));
            setThuEnd(item.split("-")[1]);
            break;
          case 4:
            setFriAvailable(true);
            setFriStart(("0" + item.split("-")[0].substr(3)).slice(-5));
            setFriEnd(item.split("-")[1]);
            break;
          default:
            break;
        }
      }
    })
  }

  const availableDayCheck = meetingSubject => {
    switch(meetingSubject) {
      case "ProjectMeeting":
        checkSubject(projectMeeting);
        break;
      case "StaffMeeting":
        checkSubject(staffMeeting);
        break;
      case "GeneralCourseAdvice":
          checkSubject(generalCourseAdvice);
          break;
      case "StudentCounsultation":
          checkSubject(studentCounsultation);
          break;
      case "MiscellaneousMeeting":
          checkSubject(miscellaneousMeeting);
          break;
      default:
        break;  
    }
  }
  return(
    <Form id="availableTimeChange" onSubmit={handleSubmit}>
      <Row form>
        <FormGroup>
          <Label for="MeetingSubject">Select meeting subject</Label>
          <Input type="select" id="MeetingSubject" onChange={handleMeetingSubjectChange}>
            <option>-----------Options----------</option>
            <option value="ProjectMeeting">Project Meeting</option>
            <option value="StaffMeeting">Staff Meeting</option>
            <option value="GeneralCourseAdvice">General Course Advice</option>
            <option value="StudentCounsultation">Student Counsultation</option>
            <option value="MiscellaneousMeeting">Miscellaneous Meeting</option>
          </Input>
        </FormGroup>  
      </Row>
      <Row form>
        <Col md={3}>
          <FormGroup>
            <Label for="availableDaysInfo">Days</Label>
            <div style={{marginTop:"5px"}}>
              <CustomInput
                          type="switch" 
                          id="Mon" 
                          label="Monday" 
                          checked={monAvailable}
                          onChange={() => setMonAvailable(!monAvailable)}/></div>
            <div style={{marginTop:"15px"}}>
              <CustomInput 
                          type="switch" 
                          id="Tue" 
                          label="Tuesday" 
                          checked={tueAvailable}
                          onChange={() => setTueAvailable(!tueAvailable)}/></div>
            <div style={{marginTop:"15px"}}>
              <CustomInput 
                          type="switch" 
                          id="Wed" 
                          label="Wednesday" 
                          checked={wedAvailable}
                          onChange={() => setWedAvailable(!wedAvailable)}/></div>
            <div style={{marginTop:"15px"}}>
              <CustomInput
                          type="switch" 
                          id="Thu" 
                          label="Thursday" 
                          checked={thuAvailable}
                          onChange={() => setThuAvailable(!thuAvailable)}/></div>
            <div style={{marginTop:"15px"}}>
              <CustomInput 
                          type="switch" 
                          id="Fri" 
                          label="Friday" 
                          checked={friAvailable}
                          onChange={() => setFriAvailable(!friAvailable)}/></div>
          </FormGroup>
        </Col>
        <Col md={4}>
          <FormGroup>
            <Label for="startTimePeriod">Select the start time</Label>
            <Input 
                  type="time" 
                  id="MonStartTime"
                  step="900" 
                  defaultValue= {monStart} min="09:00" max="16:45"
                  disabled={monAvailable ? false : true}
                  onChange={handleMonStartTime}></Input>
            <Input 
                  type="time" 
                  id="TueStartTime"
                  step="900" 
                  defaultValue={tueStart} min="09:00" max="16:45"
                  disabled={tueAvailable ? false : true}
                  onChange={handleTueStartTime}></Input>
            <Input 
                  type="time" 
                  id="WedStartTime"
                  step="900" 
                  defaultValue={wedStart} min="09:00" max="16:45"
                  disabled={wedAvailable ? false : true}
                  onChange={handleWedStartTime}></Input>
            <Input 
                  type="time" 
                  id="ThuStartTime"
                  step="900" 
                  defaultValue={thuStart} min="09:00" max="16:45"
                  disabled={thuAvailable ? false : true}
                  onChange={handleThuStartTime}></Input>
            <Input 
                  type="time" 
                  id="FriStartTime"
                  step="900" 
                  defaultValue={friStart} min="09:00" max="16:45"
                  disabled={friAvailable ? false : true}
                  onChange={handleFriStartTime}></Input>
          </FormGroup>
        </Col>
        <Col md={4}>
          <FormGroup>
            <Label for="endTimePeriod">Select the end time</Label>
            <Input 
                  type="time" 
                  id="MonEndTime"
                  step="900" 
                  defaultValue={monEnd} min="09:15" max="17:00"
                  disabled={monAvailable ? false : true}
                  onChange={handleMonEndTime}></Input>
            <Input 
                  type="time" 
                  id="TueEndTime"
                  step="900" 
                  defaultValue={tueEnd} min="09:15" max="17:00"
                  disabled={tueAvailable ? false : true}
                  onChange={handleTueEndTime}></Input>
            <Input 
                  type="time" 
                  id="WedEndTime"
                  step="900" 
                  defaultValue={wedEnd} min="09:15" max="17:00"
                  disabled={wedAvailable ? false : true}
                  onChange={handleWedEndTime}></Input>
            <Input 
                  type="time" 
                  id="ThuEndTime"
                  step="900" 
                  defaultValue={thuEnd} min="09:15" max="17:00"
                  disabled={thuAvailable ? false : true}
                  onChange={handleThuEndTime}></Input>
            <Input 
                  type="time" 
                  id="FriEndTime"
                  step="900" 
                  defaultValue={friEnd} min="09:15" max="17:00"
                  disabled={friAvailable ? false : true}
                  onChange={handleFriEndTime}></Input>
          </FormGroup>
        </Col>  
      </Row>
      <Button type="submit" hidden></Button>
    </Form>
  )
}

export default function MeeingInfoModification(props) {
  const id = props.docID;
  const [availableInfo, setAvailableInfo] = useState([]);
  useEffect(() => {
    fetchAvailableTime();
  }, []);
  //Get the calendar information from server
  const fetchAvailableTime = async () => {
    await fetch("http://localhost:5000/availabletimeinfo", {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({id})
      })
      .then(res => res.json())
      .then(data => {
        setAvailableInfo(data[0]);
      });
  }
  const [modal, setModal] = useState(false);
  const modalToggle = () => {
    setModal(!modal);
  }
  return(
    <div>
      <Button size="sm" color="warning" onClick={() => modalToggle()}>Meeting Time Modification</Button>
      <Modal isOpen={modal} toggle={modalToggle} size="lg">
        <ModalHeader toggle={() => modalToggle()}>Meeting Time Modification</ModalHeader>
        <ModalBody><MeetingModification availableInfo={availableInfo} docID={id}/></ModalBody>
        <ModalFooter>
        <Button 
          color="danger" 
          onClick={() => {modalToggle(); 
          window.location.href=`http://localhost:3000/mycalendar/${id}`}}>Close</Button>
        <Button 
          form='availableTimeChange' 
          type="submit" 
          color="success" 
          onClick={() => {alert("Changes are saved successdfully!")}}>Save Changes</Button>
        </ModalFooter>
      </Modal>
    </div>
  )
}