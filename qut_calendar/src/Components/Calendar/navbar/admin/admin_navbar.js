import React, {useState} from 'react';
import {Button, Modal, ModalHeader, ModalBody, ModalFooter,
        Navbar, NavbarBrand, NavbarToggler, Nav, NavItem, Collapse,
        NavLink} from 'reactstrap';
import MeetingInfoModification from './meetingInfo_modification';
import AvailableDaysModification from './availableDays_modification';

//This is the share application link function. Admin can use this function to let the clients interact with the calendar.
const ShareLink = props => {
  const [modal, setModal] = useState(false);
  const modalToggle = () => {
    setModal(!modal);
  }
  const shareLink = `http://localhost:3000/client/${props.docID}`;
  const copyLink = link => {
    navigator.clipboard.writeText(link);
    alert("Successful copied to clipboard");
  }
  return(
    <div>
      <Button 
        size="sm" 
        color="info" 
        onClick={() => modalToggle()}>share</Button>
      <Modal isOpen={modal} toggle={modalToggle} size="lg">
          <ModalHeader toggle={() => modalToggle()}>Share this link with your clients:</ModalHeader>
          <ModalBody>
            <div style={{fontSize:"10px"}}>{shareLink}</div>
            <p></p>
            <div style={{fontSize:'15px'}}>Click the 'Copy' button and share the link with your clients.</div>
          </ModalBody>
          <ModalFooter>
          <Button 
            color="success" 
            onClick={() => {modalToggle(); copyLink(shareLink)}}>Copy</Button>
          <Button 
            color="danger" 
            onClick={() => modalToggle()}>Cancel</Button>
          </ModalFooter>
      </Modal>
    </div>
  );
}

//Main admin navbar functions. Including: 1. Modifying which days are available for clients to make a booking.
//                                        2. Modifying the available booking period for each type of meetings.
//                                        3. Share the application link with clients.
//                                        4. Log out function.
export default function AdminNavbar(props) {
  const [navIsOpen, setNavIsOpen] = useState(false);
  const navToggle = () => {
    setNavIsOpen(!navIsOpen);
  }
  return(
    <Navbar color="primary" dark expand="md" style={{paddingTop:"0", paddingBottom:"0"}}>
      <NavbarBrand href="/">QUT</NavbarBrand>
      <NavbarToggler onClick={() => navToggle()} className="mr-2"/>
      <Collapse isOpen={navIsOpen} navbar>
        <Nav className="mr-auto" navbar> 
          <NavItem>
            <NavLink><AvailableDaysModification docID={props.docID}/></NavLink>
          </NavItem>
          <NavItem>
            <NavLink><MeetingInfoModification docID={props.docID}/></NavLink>
          </NavItem>
        </Nav>
        <Nav className="justify-content-end" navbar>
          <NavItem>
            <NavLink><ShareLink docID={props.docID}/></NavLink>
          </NavItem>
          <NavItem>
            <NavLink>
              <Button size="sm" onClick={() => {window.location.href = "http://localhost:3000/"}}>Sign Out</Button>
            </NavLink>
          </NavItem>
        </Nav>
      </Collapse>
    </Navbar>
  )
}