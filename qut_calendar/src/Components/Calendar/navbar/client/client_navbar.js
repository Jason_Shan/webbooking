import React, {useState, useEffect} from 'react';
import {Navbar, NavbarBrand, NavbarToggler, Collapse, Nav, NavItem,
        ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem} from 'reactstrap';
import {useDispatch} from 'react-redux';
import '../../style.css';


export default function ClientNavbar(props) {
  const id = props.docID;
  const [availableInfo, setAvailableInfo] = useState([]);
  useEffect(() => {
    fetchAvailableTime();
  }, []);
  //Get the calendar information from server
  const fetchAvailableTime = async () => {
    await fetch("http://localhost:5000/availabletimeinfo", {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({id})
      })
      .then(res => res.json())
      .then(data => {
        setAvailableInfo(data[0]);
      });
  }
  const dispatch = useDispatch();
  
  const saveToRedux = (index, meetingType) => {
    dispatch({type:"setMeetingType", meetingType:availableInfo[index]});
    dispatch({type:"setMeetingName", meetingName:meetingType});
  }  

  const [navIsOpen, setNavIsOpen] = useState(false);
  const [dropdownOpen, setDropDownOpen] = useState(false);
  const [meetingType, setMeetingType] = useState("Meeting Type");
  const navToggle = () => {
    setNavIsOpen(!navIsOpen);
  }
  const dropdownToggle = () => {
    setDropDownOpen(!dropdownOpen);
  }
  return(
    <Navbar color="primary" dark expand="md" style={{paddingTop:"0", paddingBottom:"0"}}>
      <NavbarBrand href="/">QUT</NavbarBrand>
      <NavbarToggler onClick={() => navToggle()} className="mr-2"/>
      <Collapse isOpen={navIsOpen} navbar>
        <Nav className="mr-auto" navbar>
          <NavItem>
            <ButtonDropdown isOpen={dropdownOpen} toggle={dropdownToggle}>
              <DropdownToggle caret color="warning">
                {meetingType}
              </DropdownToggle>
              <DropdownMenu>
                <DropdownItem 
                  onClick={()=>{setMeetingType("Project Meeting"); 
                  saveToRedux("ProjectMeeting", "Project Meeting")}}>Project Meeting
                </DropdownItem>
                <DropdownItem 
                  onClick={()=>{setMeetingType("Staff Meeting");
                  saveToRedux("StaffMeeting", "Staff Meeting")}}>Staff Meeting
                </DropdownItem>
                <DropdownItem 
                  onClick={()=>{setMeetingType("General CourseAdvice"); 
                  saveToRedux("GeneralCourseAdvice", "General CourseAdvice")}}>General CourseAdvice
                </DropdownItem>
                <DropdownItem 
                  onClick={()=>{setMeetingType("Student Counsultation");
                  saveToRedux("StudentCounsultation", "Student Counsultation")}}>Student Counsultation
                </DropdownItem>
                <DropdownItem 
                  onClick={()=>{setMeetingType("Miscellaneous Meeting"); 
                  saveToRedux("MiscellaneousMeeting", "Miscellaneous Meeting")}}>Miscellaneous Meeting
                </DropdownItem>
              </DropdownMenu>
            </ButtonDropdown>
          </NavItem>
        </Nav>
      </Collapse>
    </Navbar>
  )
}