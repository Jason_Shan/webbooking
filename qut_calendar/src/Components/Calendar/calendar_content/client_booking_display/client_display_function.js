import React, {useState, useEffect} from 'react';
import AddBooking from '../calendar_addBooking';
import {Button, Modal, ModalHeader, ModalBody, ModalFooter} from 'reactstrap';
import moment from 'moment';
import '../../style.css';

const popupMessage = id => {

  const popup = document.getElementById(id+"popup");
  popup.classList.toggle("displayPopupWindow");
}
const hidePopupWindow = (avaliable, id) => {
  const popup = document.getElementById(id+"popup");
  if(avaliable === "avaliable"){
    popup.classList.replace("displayPopupWindow", "hidePopupWindow");
  }
}

export default function ClientDisplayBooking (props) {
  let booking = "";
  props.events.forEach(event => {
    if(event.start.dateTime.split(".0000000")[0] === props.id){
      booking = event.subject;
    }
  });
  //const avaliableCheck = booking === "" ? "avaliable" : "not-avaliable";
  const avaliableCheck = id => {
    const timeFormat = "HH:mm";
    const weekDayName = moment(id).format("ddd");
    const time = moment(id.split("T")[1], timeFormat);
    let status = "not-avaliable";
    if(props.meetingInfo !== undefined){
      props.meetingInfo.split(",").forEach(day => {
        if(day.length > 7 && booking === "" && weekDayName === day.substring(0,3)){
          const startTime = moment(day.substring(3).split("-")[0], timeFormat);
          const endTime = moment(day.substring(3).split("-")[1], timeFormat);
          status = time.isBetween(startTime, endTime) || time.isSame(startTime) ? "avaliable": "not-avaliable";
        }
      })}
    return status;
  }
  const [modal, setModal] = useState(false);
  const modalToggle = avaliableCheck => {
    if(avaliableCheck !== "not-avaliable"){
      setModal(!modal);
    }
  }
  return (
    <td key={props.id}
      id={props.id}
      style={{ padding:"0px", fontSize:"11px", verticalAlign:"middle", height:"17px"}}
      className={avaliableCheck(props.id)}        
      onClick={() => {modalToggle(avaliableCheck(props.id)); hidePopupWindow(avaliableCheck(props.id), props.id)}}
      onMouseOver={() => popupMessage(props.id)}
      onMouseOut={() => popupMessage(props.id)}
      >
        <span id={props.id+"popup"} className="hidePopupWindow"
                >{props.id.split("T")[1]}
        </span>
      <Modal isOpen={modal} toggle={modalToggle} size="lg">
        <ModalHeader toggle={() => modalToggle()}>Booking Information</ModalHeader>
        <ModalBody><AddBooking id={props.id} docID={props.docID} meetingName={props.meetingName}/></ModalBody>
        <ModalFooter>
        <Button color="danger" onClick={() => modalToggle()}>Cancel</Button>
        <Button form='bookingInfo' type="submit" color="success" onClick={() => modalToggle()}>Submit</Button>
        </ModalFooter>
      </Modal>      
    </td>
  ); 
} 

