import React, {useState} from 'react';
import AddBooking from '../calendar_addBooking';
import {Button, Modal, ModalHeader, ModalBody, ModalFooter} from 'reactstrap';
import '../../style.css';

//Those two function could let the application to display the time when user move the mouse to a grid.
const popupMessage = id => {
  const popup = document.getElementById(id+"popup");
  popup.classList.toggle("displayPopupWindow");
}
const hidePopupWindow = (avaliable, id) => {
  const popup = document.getElementById(id+"popup");
  if(avaliable === "avaliable"){
    popup.classList.replace("displayPopupWindow", "hidePopupWindow");
  }
}

//This function could check every grid in the calendar, if there is a booking, display the booking. This grid is not clickable.
//Else, the admin can click the empty grid to add a booking to the calendar. A modal will pop up after clicking the empty grid
//for user to enter booking information.
export default function AdminDisplayBooking (props) {
  let booking = "";
  props.events.forEach(event => {
    if(event.start.dateTime.split(".0000000")[0] === props.id){
      booking = event.subject;
    }
  });
  const avaliableCheck = booking === "" ? "avaliable" : "not-avaliable";
  const [modal, setModal] = useState(false);
  const modalToggle = avaliableCheck => {
    if(avaliableCheck !== "not-avaliable"){
      setModal(!modal);
    }
  }
  return (
    <td key={props.id}
      id={props.id}
      style={{ padding:"0px", fontSize:"11px", verticalAlign:"middle", height:"17px"}}
      className={avaliableCheck}        
      onClick={() => {modalToggle(avaliableCheck); 
                      hidePopupWindow(avaliableCheck, props.id)}}
      onMouseOver={() => popupMessage(props.id)}
      onMouseOut={() => popupMessage(props.id)}
      >
        <span id={props.id+"popup"} className="hidePopupWindow"
                >{props.id.split("T")[1]}
        </span>
      {avaliableCheck === "not-avaliable" ? booking: null}
      <Modal isOpen={modal} toggle={modalToggle} size="lg">
        <ModalHeader toggle={() => modalToggle()}>Booking Information</ModalHeader>
        <ModalBody><AddBooking id={props.id} docID={props.docID} meetingName={"All"}/></ModalBody>
        <ModalFooter>
        <Button color="danger" onClick={() => modalToggle()}>Cancel</Button>
        <Button 
          form="bookingInfo" 
          type="submit" 
          color="success" 
          >Submit</Button>
        </ModalFooter>
      </Modal>      
    </td>
  ); 
} 

