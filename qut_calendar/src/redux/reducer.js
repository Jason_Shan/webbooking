import {combineReducers} from 'redux';

const meetingType = (state="", action) => {
  if(action.type === "setMeetingType"){
    return action.meetingType;
  }
  else{
    return state;
  }
}
const meetingName = (state="", action) => {
  if(action.type === "setMeetingName"){
    return action.meetingName;
  }
  else{
    return state;
  }
}

const timeReducer = combineReducers({
  getMeetingType: meetingType,
  getMeetingName: meetingName
});
export default timeReducer;