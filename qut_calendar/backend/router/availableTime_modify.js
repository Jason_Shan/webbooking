const express = require('express');
const router = express.Router();
const sqlite3 = require('sqlite3').verbose();

router.post('/', async (req, res, next) => {
  const info = req.body.submitInfo.split("%");
  console.log(info);
  const db = new sqlite3.Database('./dataset/tokens.db', err => {
    if (err) {
        console.log(err.message);
    }
    console.log('Server database is connected!');
    });
  
  db.serialize(() => {
    db.run(`UPDATE ${info[0]} SET ${info[1]} = "${info[2]}"`,
      err => {
        if(err) {
          console.log(err.message);
        }
        else{
          console.log(`Available time of ${info[1]} is updated.`);
        }
      }
  );
  });
  db.close();
});

module.exports = router;