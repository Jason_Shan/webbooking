var express = require('express');
var router = express.Router();
var authHelper = require('../auth');

router.get('/', (req, res, next) => {
    var uri = authHelper.getAuthUrl();
    res.send(uri);
})


module.exports = router;